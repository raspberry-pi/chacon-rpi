#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>

#ifdef __arm__
    #include <wiringPi.h>
#else
    #define LOW 0
    #define HIGH 1
    #define INPUT 0
    #define OUTPUT 1
    int wiringPiSetup(void) {return 0;}
    void pinMode (int a, int b) {a=b;}
    void delayMicroseconds(unsigned int a) {a=a;}
    void digitalWrite(int a, int b) {a=b;}
    int digitalRead(int a) {return a;}
#endif

#include "home_easy.h"
#include "buffer.h"
#include "utils.h"

unsigned int portail_timings[3][2] = {
    {1140, 380},  //  bit 0
    {380, 1140}, //  bit 1
    {380, 790},  //  start of data
};

extern unsigned char homeEasyPinOut;
extern unsigned char homeEasyPinIn;

unsigned char frame[5];
unsigned int size = 0;
/**
 * Create a complete command according to Chacon protocole
 *
 * @param id command id (refer to your remote)
 * @param section button section ('A' | 'B'  | 'C'  | 'D'  | 'G')
 * @param nb button number(1, 2, 3, 4)
 * @param on boolean for on/off
 * 
 * @return HomeEasy frame
 */
void createPortailCommand(unsigned long int id, char section)
{
	int btn = 0;
	if(section == 'B'){
		btn = 0x7B;
	}else if(section == 'A'){
		btn = 0x7D;
	}else if(section == 'C'){
		btn = 0x79;
	}else if(section == 'D'){
		btn = 0x7F;
	}


	printf(" section :%c  btn : %x  id: %x \n",section,btn,id);
	frame[0] = id;
	frame[1]=0xF7;
	frame[2]=btn;
	frame[3]=0xA1;
	frame[4]=0x00;
	size = 36;

    return;
}

/**
 * Send n times a data frame
 *
 * @param frame the data to send
 * @param repeat number of repeatition
 */
void sendPortailFrame(unsigned int repeat)
{
    unsigned int i;
    // switch to real time
    cpuMode(REAL_TIME);
    // repeat the command
    for(i=0; i<repeat; i++) {
    	sendPortailBit(START_OF_DATA);
        sendPortailBytes();
        sendPortailBit(BIT0);
	delayMicroseconds(53000);
    }
    digitalWrite(homeEasyPinOut, LOW);
    // Exit real time mode
    cpuMode(STANDARD);
}

/**
 * Send a complete command according to Chacon protocole
 *
 * @param id command id (refer to your remote)
 * @param section button section ('A' | 'B'  | 'C'  | 'D'  | 'G')
 * @param nb button number(1, 2, 3, 4)
 * @param on boolean for on/off
 * @param repeat number of repeatition
 */
void sendPortailCommand(unsigned int id, char button, unsigned char repeat)
{
    unsigned long int command;
    // build the command
    createPortailCommand(id, button);
       // send data
    sendPortailFrame(repeat);
    // release the memory
}

/**
 * Send a bit to the RF transmitter
 *
 * @param bit the bit to transmit (0 | 1 | TRIGGER0 | TRIGGER1 | END_OF_FRAME)
 */
void sendPortailBit(unsigned char bit)
{
    digitalWrite(homeEasyPinOut, LOW);
    delayMicroseconds(portail_timings[bit][0]);
    digitalWrite(homeEasyPinOut, HIGH);
    delayMicroseconds(portail_timings[bit][1]);
    digitalWrite(homeEasyPinOut, LOW);
}

/**
 * Send a byte to the RF transmitter
 *
 * @param byte the byte to transmit
 */
void sendPortailByte(unsigned char byte, unsigned int bits)
{
    int i;
    for(i=0; i < bits; i++) {
	sendPortailBit((byte & (0x01 << i)) == (0x01 << i));
   }
}

/**
 * Send the content of a buffer to the RF transmitter
 *
 * @param buffer the buffer to transmit
 */
void sendPortailBytes(void)
{
    int i;
   
    for(i = 0; i < size; i+=8 ) {
    	register unsigned int bits = (size - i >= 8) ? 8: (size - i);
        sendPortailByte(frame[i/8],bits);
    }
}


